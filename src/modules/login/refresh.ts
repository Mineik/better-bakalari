import {useEffect, useState} from "react";

export default function refreshToken(refreshToken, domain, callback){
	let tokens = {
		access: "",
		refresh: ""
	}

	fetch(`${domain}/api/login`,{
		method: "POST",
		headers: {
			"Content-Type":"application/x-www-form-urlencoded"
		},
		body: `client_id=AND&grant_type=refresh_token&refresh_token=${refreshToken}`
	}).then(res=>res.json().then(data=>{
		tokens.access = data.access_token
		tokens.refresh= data.refresh_token

		document.cookie = `access_token=${tokens.access}`
		document.cookie = `refresh_token=${tokens.refresh}`
		callback(tokens)
	}))
}