import {useEffect, useState} from "react";

export default function useLogin(username, password, domain, callback){
	console.log("getting token")
	const [tokens, updateTokens] = useState({
		access:"",
		refresh: ""
	})

	useEffect(()=>{
		console.log("running effect")
		if(tokens.access=="" && tokens.refresh=="" && !(username=="" || password=="")){
			console.log("requesting data")
		fetch(`${domain}/api/login`,{
			method: "POST",
			headers: {
				"Content-Type":"application/x-www-form-urlencoded"
			},
			body: `client_id=ANDR&grant_type=password&username=${username}&password=${password}`
		}).then(res=>res.json().then(data=>{
			console.log(data)
			updateTokens({
				access: data.access_token,
				refresh: data.refresh_token
			})
		}))}
		else{
			console.log(tokens)
			callback(tokens)
		}
	})
}