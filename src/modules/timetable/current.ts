import {useEffect, useState} from "react";
import {timetable, timetableDay, periodsHead, timetablePeriod } from "./types";
import refreshToken from "../login/refresh";
import {findCookie} from "../../util/findCookie";

export default function useRozvrhCurrent(token:string, date: string /*ISO-8601 compliable*/, domain: string, updateDataCallback){
	console.log("Getting current timetable")
	const [updatableData, updateThoseBithches] = useState({
		periods: [],
		days: [],
	})

	useEffect(()=>{
		if(updatableData.periods.length > 0 && updatableData.days.length > 0) updateDataCallback(updatableData)
		else if(updatableData.periods.length == 0 && updatableData.days.length == 0){
			console.log("getting data")
			fetchRozvrhCurrent(token, date, domain).then(data=>{
				// @ts-ignore
				if(!data.Message){ //if we arent getting any error from bakaláři backend
					//@ts-ignore
					const periods: periodsHead[] = data.Hours.map(object=>{
						return {
							id: object.Id,
							caption: object.Caption,
							time: {
								begin: object.BeginTime,
								end: object.EndTime
							}
						}
					})
					//@ts-ignore
					const timetableDays: timetableDay[] = data.Days.map(e=>{
						const hodiny:timetablePeriod[] = e.Atoms.map(f=>{
							if(f.Change === null)
								return {
									classId: f.HourId,
									subject: {
										id: f.SubjectId,
										//@ts-ignore
										abbr: data.Subjects.find(i=> i.Id == f.HourId).Abbrev,
										//@ts-ignore
										name: data.Subjects.find(i=> i.Id == f.HourId).Name
									},
									teacher: {
										//@ts-ignore
										name: data.Teachers.find(i=>i.Id == f.TeacherId).Name,
										//@ts-ignore
										abbr: data.Teachers.find(i=>i.Id == f.TeacherId).Abbrev
									},
									classRoom: {
										//@ts-ignore
										name: data.Rooms.find(i=>i.Id == f.RoomId).Name,
										//@ts-ignore
										abbr: data.Rooms.find(i=>i.Id == f.roomId).Abbrev
									},

									isHomeWorkDue: f.HomeworkIds > 0,
								}
							else{
								return {
									classId: f.HourId,
									subject: {
										id: f.SubjectId,
										//@ts-ignore
										abbr: data.Subjects.find(i=> i.Id == f.HourId).Abbrev,
										//@ts-ignore
										name: data.Subjects.find(i=> i.Id == f.HourId).Name
									},
									teacher: {
										//@ts-ignore
										name: data.Teachers.find(i=>i.Id == f.TeacherId).Name,
										//@ts-ignore
										abbr: data.Teachers.find(i=>i.Id == f.TeacherId).Abbrev
									},
									classRoom: {
										//@ts-ignore
										name: data.Rooms.find(i=>i.Id == f.RoomId).Name,
										//@ts-ignore
										abbr: data.Rooms.find(i=>i.Id == f.roomId).Abbrev
									},

									isHomeWorkDue: f.HomeworkIds > 0,

									change: {
										type: f.Change.ChangeType,
										description: f.Change.Description
									}
								}
							}
						})

						return{
							day: {
								dayOfWeek: e.DayOfWeek,
								date: e.Date,
								dayType: e.DayType
							},
							classes: hodiny
						}
					})
					updateThoseBithches({
						periods: periods,
						days: timetableDays
					})
				}

			}).catch(e=>{
				if(typeof e == "object"){
					if(e.code == 401){
						refreshToken(findCookie("refresh_token"), domain,({newToken, newRefreshToken})=>{
							useRozvrhCurrent(newToken, date, domain, updateDataCallback)
						})
					}
				}else console.log(e);
			})
	}
	})


}


 async function fetchRozvrhCurrent(token: string, date: string /*ISO-8601 compliable*/, domain: string){
	return new Promise((resolve, reject)=>{
		if(token == "") {
			reject("please provide valid token")
			return
		}
		else console.log("valid token received")
		 fetch(`/fetch/timetableCurrent.php?date=${date}&school=${domain}`,{
		 	mode: "cors",
			 headers: {
				 Authorization: `bearer ${token}`,
				 "Content-Type": "application/x-www-form-urlencoded"
			 },
			 method: "GET",
		 }).then(res=> {
		 	console.log(res.status)
		 	if(res.status == 401){
		 		reject({
					status: 401,
					message: "refresh plz"
				})
			}
		 	res.text().then(data=> {
				 try{
					 console.log(data)
					 resolve(data)
				 }catch(e){
					 reject(e)
				 }
			 })
		 })

	 })
}
