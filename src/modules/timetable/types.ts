export class timetablePeriod {
	classId: number
	subject: {
		id: string
		abbr: string
		name: string
		theme: string
	}
	teacher: {
		name: string
		abbr: string
	}
	classRoom: {
		id: string
		abbr: string
		name: string
	}

	change?:{
		type: string
		description: string
	}
	isHomeworkDue: boolean
}

export class timetableDay{
	day: {
		dayOfWeek: number
		date: string
		dayType: string
	}

	classes: timetablePeriod[]
}

export class periodsHead{
	id: number
	caption: string
	time: {
		begin: string
		end: string
	}
}

export class timetable{
	periods: periodsHead[]
	days: timetableDay[]
}