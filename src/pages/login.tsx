import React from "react";
import Head from "next/head"
import useLogin from "../modules/login/login";
import Link from "next/Link"
import {findCookie} from "../util/findCookie";

export default function Login(){
	const [tokens, updateTokens] = React.useState({
		access: "",
		refresh: ""
	})
	const [creds, updateCreds] = React.useState({
		uname: "",
		pword: ""
	})
	console.log(creds)

	useLogin(creds.uname, creds.pword, "https://bakalari.spseplzen.cz", updateTokens)

	React.useEffect(()=>{
		if(tokens.access != "" && tokens.refresh != ""){
			console.log("writing cookies...")
			document.cookie = `access_token=${tokens.access}`
			document.cookie = `refresh_token=${tokens.refresh}`
			console.log(findCookie("access_token"))
		}

	},[tokens])

	return(
		<>
			<Head>
				<title>Login | Lepší bakaláři</title>
			</Head>

			<div>
				<label htmlFor={"username"}>Uživatelské jméno</label>
				<input name={"username"} id={"uname"}/>
				<label htmlFor={"password"}>Heslo</label>
				<input name={"password"} type={"password"} id={"pword"}/>
				<button onClick={()=>{
					const username = (document.getElementById("uname") as HTMLInputElement).value
					const password = (document.getElementById("pword") as HTMLInputElement).value
					updateCreds({
						uname: username,
						pword: password
					})
				}}>Loguuuuj</button>
				{(tokens.access != "" && tokens.refresh != "")? <Link href={`/user/timetable`}><a>Lognuto, jdi dál</a></Link>:""}
			</div>
		</>
	)
}