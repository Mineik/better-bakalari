import React from "react"
import {timetable} from "modules/timetable/types";
import useRozvrhCurrent from "modules/timetable/current";
import {useRouter} from "next/router";
import {findCookie} from "../../util/findCookie";


export default function Timetable() {
	console.count("render")
	const[token, updateToken] = React.useState("");
	const[button, updateClicks] = React.useState(0)
	let school = ""
	React.useEffect(()=>{
		const router = new URL(window.location.href)
		updateToken(findCookie("access_token"))
		school = "https://bakalari.spseplzen.cz"
	}, )
	const [rozvrh, updateRozvrh] = React.useState({})
	console.log(token)
	const date = new Date();
	useRozvrhCurrent(token,`${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`, "https://bakalari.spseplzen.cz", updateRozvrh)


	return(
		<div>
			{rozvrh.toString()}
			<button onClick={()=>updateClicks(button+1)}>Prostě přerendruj stránku</button>
		</div>
	)
}