export function findCookie(cookieName){
	return document.cookie.split("; ").find(row=>row.startsWith(cookieName)).split("=")[1]
}