const withLess = require("@zeit/next-less")

module.exports = withLess({
        devIndicators: {
            autoPrerender: false,
        },
        target: 'serverless',
        poweredByHeader: false
     })